package com.hackatonbbva.geofertas.data.api

import com.hackatonbbva.geofertas.models.LocalOfertaRequest
import com.hackatonbbva.geofertas.models.OfertaCercanaRequest
import com.hackatonbbva.geofertas.models.Ofertas
import com.hackatonbbva.geofertas.models.OfertasDetalles
import retrofit2.Call
import retrofit2.http.*

interface OfertaApi {

    @POST("oferta/getOfertasCercanas")
    fun getOfertas(@Header("Authorization") authHeader: String, @Body ofertaRequest: OfertaCercanaRequest): Call<Ofertas>

    @POST("oferta/getOferta/")
    fun getOfertaDetalle(@Header("Authorization") authHeader: String, @Body ofertaRequest: LocalOfertaRequest): Call<OfertasDetalles>

}