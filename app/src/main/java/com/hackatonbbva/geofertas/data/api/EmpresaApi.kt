package com.hackatonbbva.geofertas.data.api

import com.hackatonbbva.geofertas.models.EmpresaCercanaRequest
import com.hackatonbbva.geofertas.models.Empresas
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface EmpresaApi {

    @POST("empresa/getEmpresasCercanas")
    fun getEmpresas(@Header("Authorization") authHeader: String, @Body empresaRequest: EmpresaCercanaRequest): Call<Empresas>

}