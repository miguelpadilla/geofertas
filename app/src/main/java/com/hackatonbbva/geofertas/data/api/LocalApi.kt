package com.hackatonbbva.geofertas.data.api

import com.hackatonbbva.geofertas.models.*
import retrofit2.Call
import retrofit2.http.*

interface LocalApi {

    @POST("local/getLocalesCercanos")
    fun getLocalesCercanos(@Header("Authorization") authHeader: String, @Body request: LocalOfertaRequest): Call<Locales>

}