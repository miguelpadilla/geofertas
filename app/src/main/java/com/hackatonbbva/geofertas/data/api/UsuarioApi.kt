package com.hackatonbbva.geofertas.data.api

import com.hackatonbbva.geofertas.models.JwtResponse
import com.hackatonbbva.geofertas.models.LoginDto
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UsuarioApi {

    @POST("usuario/login")
    fun login(@Body user: LoginDto): Call<JwtResponse>

}