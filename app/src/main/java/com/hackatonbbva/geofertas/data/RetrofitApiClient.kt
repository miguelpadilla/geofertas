package com.hackatonbbva.geofertas.data

import com.google.gson.GsonBuilder
import com.hackatonbbva.geofertas.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitApiClient {

    private fun createRetrofitInstance(): Retrofit {

        val gson = GsonBuilder()
            .setDateFormat("dd/MM/yyyy")
            .create()

        return Retrofit.Builder()
            .baseUrl(Constants.BASE_API_PATH)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    fun <S> createRetrofitService(service: Class<S>): S {
        return createRetrofitInstance().create(service)
    }

}