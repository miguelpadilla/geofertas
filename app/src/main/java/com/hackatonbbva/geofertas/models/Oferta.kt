package com.hackatonbbva.geofertas.models

import java.io.Serializable

data class Oferta (
    val idOferta : Long,
    val ofertaDescripcion : String,
    val ofertaImagen : String,
    val validoDesdeOferta : String,
    val validoHastaOferta : String,
    val nuevoOferta : Int,
    val diasRestantes: Int,
    val listaLocales: ArrayList<LocalCercanoResponse>
) : Serializable