package com.hackatonbbva.geofertas.models

data class Ofertas(
    val data: ArrayList<Oferta>
)