package com.hackatonbbva.geofertas.models

data class EmpresaCercanaRequest (
    val numeroDocumento:String,
    val latitud :Double,
    val longitud :Double,
    val pagina :Int
)