package com.hackatonbbva.geofertas.models

data class Locales (
    val data: ArrayList<LocalCercanoResponse>
)