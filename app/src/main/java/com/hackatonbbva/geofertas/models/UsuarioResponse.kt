package com.hackatonbbva.geofertas.models

data class UsuarioResponse(
    val idUsuario: Long,
    val numeroDocumentoUsuario: String,
    val nombresUsu: String,
    val apellidoPatUsuario: String,
    val apellidoMatUsuario: String
)