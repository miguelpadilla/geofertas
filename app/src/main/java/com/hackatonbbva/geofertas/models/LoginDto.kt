package com.hackatonbbva.geofertas.models

data class LoginDto (
    val username: String
)