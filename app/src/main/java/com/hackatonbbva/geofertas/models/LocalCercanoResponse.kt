package com.hackatonbbva.geofertas.models

import java.io.Serializable

data class LocalCercanoResponse (
    val idLocal : Long,
    val nombreLocal : String,
    val referenciaLocal : String,
    val direccionLocal : String
) : Serializable