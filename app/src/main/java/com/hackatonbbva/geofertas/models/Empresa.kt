package com.hackatonbbva.geofertas.models

import java.io.Serializable

data class Empresa (
    val idEmpresa : Long,
    val nombreEmpresa : String,
    val direccionEmpresa: String,
    val imagenEmpresa : String
) : Serializable