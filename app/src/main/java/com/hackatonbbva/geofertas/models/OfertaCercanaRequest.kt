package com.hackatonbbva.geofertas.models

data class OfertaCercanaRequest (
    val numeroDocumento:String,
    val idLocal :Long?,
    val latitud :Double,
    val longitud :Double,
    val pagina :Int
)