package com.hackatonbbva.geofertas.models

data class LocalOfertaRequest (
    val numeroDocumento:String,
    val idEmpresa:Long?,
    val idOferta:Long?,
    val latitud :Double,
    val longitud :Double,
    val pagina :Int
)