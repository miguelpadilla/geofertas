package com.hackatonbbva.geofertas.models

data class JwtResponse (
    val mensaje: String,
    val token: String,
    val status: Int,
    val user: UsuarioResponse
)