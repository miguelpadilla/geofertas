package com.hackatonbbva.geofertas.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.models.Oferta
import com.hackatonbbva.geofertas.ui.DetailActivity
import com.hackatonbbva.geofertas.ui.OfertasActivity
import com.hackatonbbva.geofertas.utils.Commons

class OfertasAdapter(private val context: Context) : RecyclerView.Adapter<OfertasAdapter.ViewHolder>() {

    private var ofertas: ArrayList<Oferta>? = null

    init {
        this.ofertas = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfertasAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.oferta_item, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cvOferta: CardView? = null
        var tvOfertaName: TextView? = null

        init {
            cvOferta = itemView.findViewById(R.id.cvOfertaItem)
            tvOfertaName = itemView.findViewById(R.id.tvOfertaItem)

        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvOfertaName!!.text = ofertas!![position].ofertaDescripcion

        holder.cvOferta!!.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("selected_offer", ofertas!![position])
            ContextCompat.startActivity(context, intent, null)
        }

    }

    override fun getItemCount(): Int {
        return ofertas!!.size
    }


    fun addOfertas(ofertas: List<Oferta>){
        this.ofertas?.clear()
        this.ofertas?.addAll(ofertas)
        notifyDataSetChanged()
    }

    fun getOfertas(): ArrayList<Oferta>?{
        return this.ofertas
    }
}