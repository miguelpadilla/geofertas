package com.hackatonbbva.geofertas.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.models.Oferta
import com.hackatonbbva.geofertas.ui.DetailActivity
import com.hackatonbbva.geofertas.utils.Commons

class HomePagerAdapter(var context: Context, var ofertas: List<Oferta>?) : PagerAdapter() {

    private lateinit var inflater: LayoutInflater

    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 == p1
    }

    override fun getCount(): Int {
        return ofertas!!.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.pager_item, container, false)

        val ivPager: ImageView
        ivPager = view.findViewById(R.id.iv_slider_partner)

        val progress = Commons.createCircularProgressDrawable(context, 50F, 10F)
        progress.start()

        Glide.with(context)
            .load(ofertas!![position].ofertaImagen)
            .centerCrop()
            .placeholder(progress)
            .into(ivPager)

        ivPager.setOnClickListener{
            openDetails(ofertas!![position])
        }

        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }

    private fun openDetails(oferta: Oferta){
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("selected_offer", oferta)
        ContextCompat.startActivity(context, intent, null)
    }

}