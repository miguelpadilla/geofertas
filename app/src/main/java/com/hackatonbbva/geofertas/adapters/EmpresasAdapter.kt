package com.hackatonbbva.geofertas.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.models.Empresa
import com.hackatonbbva.geofertas.ui.LocalesActivity

class EmpresasAdapter(private val context: Context) : RecyclerView.Adapter<EmpresasAdapter.ViewHolder>() {

    private var empresas: ArrayList<Empresa>? = null

    init {
        this.empresas = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpresasAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.empresa_item, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cvEmpresa: CardView? = null
        var tvEmpresaName: TextView? = null

        init {
            cvEmpresa = itemView.findViewById(R.id.cvEmpresaItem)
            tvEmpresaName = itemView.findViewById(R.id.tvEmpresaItem)

        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvEmpresaName!!.text = empresas!![position].nombreEmpresa

        holder.cvEmpresa!!.setOnClickListener {
            val intent = Intent(context, LocalesActivity::class.java)
            intent.putExtra("selected_empresa", empresas!![position])
            startActivity(context, intent, null)
        }

    }

    override fun getItemCount(): Int {
        return empresas!!.size
    }


    fun addEmpresas(empresas: List<Empresa>){
        this.empresas?.clear()
        this.empresas?.addAll(empresas)
        notifyDataSetChanged()
    }

    fun getEmpresas(): ArrayList<Empresa>?{
        return this.empresas
    }
}