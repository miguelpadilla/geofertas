package com.hackatonbbva.geofertas.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.models.Empresa
import com.hackatonbbva.geofertas.ui.EmpresasActivity
import com.hackatonbbva.geofertas.ui.LocalesActivity
import de.hdodenhof.circleimageview.CircleImageView

class EmpresasMiniAdapter(private val context: Context) : RecyclerView.Adapter<EmpresasMiniAdapter.ViewHolder>() {

    private var empresas: ArrayList<Empresa>? = null

    init {
        this.empresas = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpresasMiniAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.empresamini_item, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvCategory: TextView? = null
        var ivCategory: CircleImageView? = null
        var frameCategory: FrameLayout? = null

        init {
            tvCategory = itemView.findViewById(R.id.tv_category)
            ivCategory = itemView.findViewById(R.id.iv_category)
            frameCategory = itemView.findViewById(R.id.frame_category)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvCategory!!.text = empresas!![position].nombreEmpresa

        Glide.with(context)
            .load(empresas!![position].imagenEmpresa)
            .centerCrop()
            .into(holder.ivCategory!!)

        holder.frameCategory!!.setOnClickListener {
            if(empresas!![position].idEmpresa == 0L){
                val intent = Intent(context, EmpresasActivity::class.java)
                startActivity(context, intent, null)
            } else {
                val intent = Intent(context, LocalesActivity::class.java)
                intent.putExtra("selected_empresa", empresas!![position])
                startActivity(context, intent, null)
            }
        }
    }

    override fun getItemCount(): Int {
        return empresas!!.size
    }


    fun addEmpresas(empresas: List<Empresa>){
        this.empresas?.clear()
        this.empresas?.addAll(empresas)
        notifyDataSetChanged()
    }

    fun getEmpresas(): ArrayList<Empresa>?{
        return this.empresas
    }
}