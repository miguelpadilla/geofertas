package com.hackatonbbva.geofertas.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.models.LocalCercanoResponse
import com.hackatonbbva.geofertas.ui.OfertasActivity

class LocalesAdapter(private val context: Context, private val empresaName: String) : RecyclerView.Adapter<LocalesAdapter.ViewHolder>() {

    private var locales: ArrayList<LocalCercanoResponse>? = null

    init {
        this.locales = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocalesAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.local_item, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cvLocal: CardView? = null
        var tvLocalName: TextView? = null
        var tvLocalRef: TextView? = null

        init {
            cvLocal = itemView.findViewById(R.id.cvLocalItem)
            tvLocalName = itemView.findViewById(R.id.tvLocalItem)
            tvLocalRef = itemView.findViewById(R.id.tvRefLocalItem)

        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvLocalName!!.text = locales!![position].nombreLocal
        holder.tvLocalRef!!.text = locales!![position].referenciaLocal

        holder.cvLocal!!.setOnClickListener {
            val intent = Intent(context, OfertasActivity::class.java)
            intent.putExtra("selected_local", locales!![position])
            intent.putExtra("empresaName", empresaName)
            ContextCompat.startActivity(context, intent, null)
        }

    }

    override fun getItemCount(): Int {
        return locales!!.size
    }


    fun addLocales(locales: List<LocalCercanoResponse>){
        this.locales?.clear()
        this.locales?.addAll(locales)
        notifyDataSetChanged()
    }

    fun getLocales(): ArrayList<LocalCercanoResponse>?{
        return this.locales
    }
}