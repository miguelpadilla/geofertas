package com.hackatonbbva.geofertas.utils

object Constants {
    const val BASE_API_PATH = "https://spring-geofertas-mpadillaespino.cloud.okteto.net/api/"
    const val IMG_FADE_DURATION = 275
}