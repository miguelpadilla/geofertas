package com.hackatonbbva.geofertas.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location
import android.location.LocationListener;
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat;

class GeoLocalizacion {
    inner class Localizacion {
        var longitud: Double = 0.0
        var latitud: Double = 0.0
    }

    fun getActualPosition (activity :Activity): Localizacion {
        val loc = Localizacion()

        val locationListener = object:LocationListener {
            override fun onLocationChanged(location: Location?) { }
            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) { }
            override fun onProviderEnabled(provider: String?) { }
            override fun onProviderDisabled(provider: String?)  { }
        }

        if (ActivityCompat.checkSelfPermission(activity.applicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(activity.applicationContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            val locManager: LocationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val providers: List<String> = locManager.getProviders(true)

            for(provider: String in providers){
                if(locManager.isProviderEnabled(provider)){
                    locManager.requestLocationUpdates(provider, 1000L, 5F, locationListener)
                    try {
                        val localizacion: Location? = locManager.getLastKnownLocation(provider)
                        if (localizacion != null){
                            loc.longitud = localizacion.longitude
                            loc.latitud = localizacion.latitude
                        }
                    } catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            }
            locManager.removeUpdates(locationListener)
        }
        return loc
    }
}