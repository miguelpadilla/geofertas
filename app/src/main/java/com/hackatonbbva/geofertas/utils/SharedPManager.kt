package com.hackatonbbva.geofertas.utils

import android.content.Context

class SharedPManager private constructor(private val mCtx: Context) {

    fun saveDocumentForLogin(document: String) {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("document_for_login", document)
        editor.apply()
    }

    val getDocumentForLogin: String
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("document_for_login", "")!!
        }


    fun saveLoginToken(login_access_token: String) {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("login_access_token", login_access_token)
        editor.apply()
    }

    val getLoginToken: String
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("login_access_token", null)!!
        }

    fun saveUserName(username: String) {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("user_name", username)
        editor.apply()
    }

    val getUsername: String
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getString("user_name", null)!!
        }

    fun saveOnboardingFlag(flag: Boolean) {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean("onboarding_flag", flag)
        editor.apply()
    }

    val getOnboardingFlag: Boolean
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getBoolean("onboarding_flag", false)
        }

    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private const val SHARED_PREF_NAME = "my_shared_preff"
        private var mInstance: SharedPManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPManager {
            if (mInstance == null) {
                mInstance = SharedPManager(mCtx)
            }
            return mInstance as SharedPManager
        }
    }
}