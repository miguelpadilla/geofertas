package com.hackatonbbva.geofertas.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.Gravity
import com.crowdfire.cfalertdialog.CFAlertDialog
import com.hackatonbbva.geofertas.R
import android.provider.Settings
import androidx.swiperefreshlayout.widget.CircularProgressDrawable

object Commons {

    fun showLoader(context: Context): CFAlertDialog.Builder {
        return CFAlertDialog.Builder(context, R.style.CFDialogLoader)
            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
            .setHeaderView(R.layout.progress_dialog)
            .setTextGravity(Gravity.CENTER)
            .setOuterMargin(150)
            .setCancelable(false)
            .setTitle(context.getString(R.string.msg_wait))
    }

    fun openAppSettings(context: Context){
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", context.packageName, null)
        intent.data = uri
        context.startActivity(intent)
    }

    fun createCircularProgressDrawable(context: Context, radius: Float, strokeWidth: Float): CircularProgressDrawable{
        val progress =  CircularProgressDrawable(context)
        progress.setColorSchemeColors(context.getColor(R.color.colorAccent))
        progress.strokeWidth = strokeWidth
        progress.centerRadius = radius
        return progress
    }

}