package com.hackatonbbva.geofertas.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.adapters.EmpresasMiniAdapter
import com.hackatonbbva.geofertas.adapters.HomePagerAdapter
import com.hackatonbbva.geofertas.data.RetrofitApiClient
import com.hackatonbbva.geofertas.data.api.EmpresaApi
import com.hackatonbbva.geofertas.data.api.OfertaApi
import com.hackatonbbva.geofertas.models.*
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.GeoLocalizacion
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.toolbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : AppCompatActivity() {

    private lateinit var geoLoc: GeoLocalizacion
    private lateinit var empresaService: EmpresaApi
    private lateinit var ofertaService: OfertaApi
    private lateinit var empresasAdapter: EmpresasMiniAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        geoLoc = GeoLocalizacion()
        val loc = geoLoc.getActualPosition(this)
        empresaService = RetrofitApiClient.createRetrofitService(EmpresaApi::class.java)
        ofertaService = RetrofitApiClient.createRetrofitService(OfertaApi::class.java)
        initRecycler()
        tvUsername.text = String.format(getString(R.string.request_location_head), SharedPManager.getInstance(this).getUsername)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        loadEmpresas(loc.latitud, loc.longitud)
        loadOferts(loc.latitud, loc.longitud)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.config, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        if (itemId == R.id.config_user) {
            finish()
        }
        return true
    }

    private fun initRecycler(){
        empresasAdapter = EmpresasMiniAdapter(this)
        recyclerEmpresas.adapter = empresasAdapter
        recyclerEmpresas.setHasFixedSize(true)
        recyclerEmpresas.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun loadEmpresas(latitude: Double, longitude: Double){
        val alert = Commons.showLoader(this).show()
        val empresaCercanaRequest = EmpresaCercanaRequest(SharedPManager.getInstance(this).getDocumentForLogin, latitude, longitude, 0)
        val token = SharedPManager.getInstance(this).getLoginToken
        empresaService.getEmpresas("Bearer $token", empresaCercanaRequest).enqueue(object:
            Callback<Empresas> {
            override fun onFailure(call: Call<Empresas>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<Empresas>, response: Response<Empresas>) {
                alert.dismiss()
                val empresas: Empresas? = response.body()

                if(empresas != null){
                    if(empresas.data.count() > 0){
                        val todos = Empresa(0L, "TODOS", "", "https://www.mipatente.com/wp-content/uploads/2011/10/Car-Brands11.jpg")
                        empresas.data.add(0, todos)
                        empresasAdapter.addEmpresas(empresas.data)
                    }
                }
            }
        })
    }

    private fun loadOferts(latitude: Double, longitude: Double){
        val alert = Commons.showLoader(this).show()
        val ofertaRequest = OfertaCercanaRequest(SharedPManager.getInstance(this).getDocumentForLogin, null, latitude, longitude, 0)
        val token = SharedPManager.getInstance(this).getLoginToken
        ofertaService.getOfertas("Bearer $token", ofertaRequest).enqueue(object:
            Callback<Ofertas> {
            override fun onFailure(call: Call<Ofertas>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<Ofertas>, response: Response<Ofertas>) {
                alert.dismiss()
                val ofertas: Ofertas? = response.body()
                if(ofertas != null){
                    if(ofertas.data.count() > 0){
                        homePager.adapter = HomePagerAdapter(this@HomeActivity, ofertas.data)
                        pageIndicator.attachToPager(homePager)
                    }
                }
            }
        })
    }


}