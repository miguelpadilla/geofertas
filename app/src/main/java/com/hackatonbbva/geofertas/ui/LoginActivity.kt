package com.hackatonbbva.geofertas.ui

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.crowdfire.cfalertdialog.CFAlertDialog
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.data.RetrofitApiClient
import com.hackatonbbva.geofertas.data.api.UsuarioApi
import com.hackatonbbva.geofertas.models.JwtResponse
import com.hackatonbbva.geofertas.models.LoginDto
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    private val documentTypes: Array<String> = arrayOf("DNI", "CE", "PASAPORTE")
    private lateinit var userService: UsuarioApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        userService = RetrofitApiClient.createRetrofitService(UsuarioApi::class.java)

        loadDocumentTypes()
        val documentnumber = SharedPManager.getInstance(this).getDocumentForLogin
        if(documentnumber.isNotEmpty()){
            etDocNumber.setText(documentnumber)
            etDocNumber.setSelection(documentnumber.length)
        }

        btnStart.setOnClickListener{
            login()
        }
    }

    private fun login() {
        if(etDocNumber.text.toString().isEmpty()){
            Toast.makeText(this, getString(R.string.error_message_invalid_docnumber), Toast.LENGTH_SHORT).show()
            return
        }
        if(etDocNumber.text.toString().length < 8){
            Toast.makeText(this, getString(R.string.error_message_invalid_docnumber), Toast.LENGTH_SHORT).show()
            return
        }

        if (!checkGPSEnabled()){
            Toast.makeText(this, getString(R.string.error_no_gps), Toast.LENGTH_SHORT).show()
            return
        }

        val alert = Commons.showLoader(this).show()

        val user = LoginDto(etDocNumber.text.toString().trim())
        userService.login(user).enqueue(object: Callback<JwtResponse>{
            override fun onFailure(call: Call<JwtResponse>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<JwtResponse>, response: Response<JwtResponse>) {
                alert.dismiss()
                if (response.code() != 200){
                    CFAlertDialog.Builder(this@LoginActivity, R.style.CFDialog)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                        .setTextGravity(Gravity.CENTER)
                        .setTitle(getString(R.string.title_error))
                        .setMessage(getString(R.string.msg_error_login))
                        .addButton(getString(R.string.button_entendido), ContextCompat.getColor(this@LoginActivity, R.color.colorAccent), -1, CFAlertDialog.CFAlertActionStyle.DEFAULT,
                            CFAlertDialog.CFAlertActionAlignment.JUSTIFIED) { dialog, _ ->
                            dialog.dismiss()
                        }.show()
                } else {
                    processLogin(response.body())
                }
            }
        })

    }

    private fun processLogin(body: JwtResponse?) {
        SharedPManager.getInstance(this@LoginActivity).saveDocumentForLogin(etDocNumber.text.toString())
        SharedPManager.getInstance(this@LoginActivity).saveUserName(body!!.user.nombresUsu.toLowerCase().capitalize())
        SharedPManager.getInstance(this@LoginActivity).saveLoginToken(body.token)

        if(ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            || ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            startActivity(Intent(this, RequestLocationActivity::class.java))
        } else {
            startActivity(Intent(this, HomeActivity::class.java))
        }
        finish()
    }

    private fun loadDocumentTypes() {
        val array = ArrayAdapter(
            baseContext,
            android.R.layout.simple_spinner_item,
            documentTypes
        )
        array.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDocType.adapter = array
    }

    private fun checkGPSEnabled(): Boolean{
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

}