package com.hackatonbbva.geofertas.ui

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import androidx.core.content.ContextCompat
import com.crowdfire.cfalertdialog.CFAlertDialog
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_request_location.*

class RequestLocationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_location)

        tvHead.text = String.format(getString(R.string.request_location_head), SharedPManager.getInstance(this).getUsername)
        btnEnableLocation.setOnClickListener {
            enableLocation()
        }
    }

    private fun enableLocation() {
        requestPermissions(arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION), 0)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == 0){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                goToHome()
            } else {
                CFAlertDialog.Builder(this, R.style.CFDialogLoader)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                    .setTextGravity(Gravity.CENTER)
                    .setTitle(getString(R.string.title_ask_loc_permisson))
                    .setMessage(getString(R.string.msg_loc_permission_denied))
                    .addButton(getString(R.string.button_open_config), ContextCompat.getColor(this, R.color.colorAccent), -1, CFAlertDialog.CFAlertActionStyle.DEFAULT,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED) { dialog, _ ->
                        Commons.openAppSettings(this)
                        tvMessage.text = getString(R.string.request_location_foot_error)
                        dialog.dismiss()
                    }.show()
            }
        }
    }

    private fun goToHome(){
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

}