package com.hackatonbbva.geofertas.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.adapters.OfertasAdapter
import com.hackatonbbva.geofertas.data.RetrofitApiClient
import com.hackatonbbva.geofertas.data.api.OfertaApi
import com.hackatonbbva.geofertas.models.Empresa
import com.hackatonbbva.geofertas.models.LocalCercanoResponse
import com.hackatonbbva.geofertas.models.OfertaCercanaRequest
import com.hackatonbbva.geofertas.models.Ofertas
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.GeoLocalizacion
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_ofertas.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OfertasActivity : AppCompatActivity() {

    private lateinit var geoLoc: GeoLocalizacion
    private lateinit var ofertaService: OfertaApi
    private lateinit var ofertasAdapter: OfertasAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ofertas)
        val selectedLocal: LocalCercanoResponse = intent?.extras?.get("selected_local") as LocalCercanoResponse
        val empresaName = intent.getStringExtra("empresaName")
        geoLoc = GeoLocalizacion()
        val posicion = geoLoc.getActualPosition(this)
        ofertaService = RetrofitApiClient.createRetrofitService(OfertaApi::class.java)
        initToolbar()
        initRecycler()
        loadOferts(posicion.latitud, posicion.longitud, selectedLocal.idLocal)
        tvCategoryTitle.text = "     $empresaName"
        tv_oferta.text = selectedLocal.nombreLocal
    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
    }

    private fun initRecycler(){
        ofertasAdapter = OfertasAdapter(this)
        recyclerOfertas.adapter = ofertasAdapter
        recyclerOfertas.setHasFixedSize(true)
        recyclerOfertas.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun loadOferts(latitude: Double, longitude: Double, idLocal: Long){
        val alert = Commons.showLoader(this).show()
        val ofertaRequest = OfertaCercanaRequest(SharedPManager.getInstance(this).getDocumentForLogin, idLocal, latitude, longitude, 0)
        val token = SharedPManager.getInstance(this).getLoginToken
        ofertaService.getOfertas("Bearer $token", ofertaRequest).enqueue(object:
            Callback<Ofertas> {
            override fun onFailure(call: Call<Ofertas>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<Ofertas>, response: Response<Ofertas>) {
                alert.dismiss()
                val ofertas: Ofertas? = response.body()
                if(ofertas != null){
                    if(ofertas.data.count() > 0){
                        ofertasAdapter.addOfertas(ofertas.data)
                    }
                }
            }
        })
    }


}