package com.hackatonbbva.geofertas.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.crowdfire.cfalertdialog.CFAlertDialog
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.data.RetrofitApiClient
import com.hackatonbbva.geofertas.data.api.OfertaApi
import com.hackatonbbva.geofertas.models.*
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.GeoLocalizacion
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_detail.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailActivity : AppCompatActivity() {

    private lateinit var ofertaService: OfertaApi
    private lateinit var geoLoc: GeoLocalizacion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        geoLoc = GeoLocalizacion()
        val loc = geoLoc.getActualPosition(this)

        ofertaService = RetrofitApiClient.createRetrofitService(OfertaApi::class.java)
        val selectedOffer: Oferta = intent?.extras?.get("selected_offer") as Oferta
        loadDetail(selectedOffer.idOferta, loc.latitud, loc.longitud)

        btnOferta.setOnClickListener{
            CFAlertDialog.Builder(this, R.style.CFDialog)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTextGravity(Gravity.CENTER)
                .setTitle(getString(R.string.title_oferta))
                .setMessage(getString(R.string.message_oferta))
                .addButton(getString(R.string.button_entendido), ContextCompat.getColor(this, R.color.colorAccent), -1, CFAlertDialog.CFAlertActionStyle.DEFAULT,
                    CFAlertDialog.CFAlertActionAlignment.JUSTIFIED) { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }.show()
        }

    }

    private fun loadDetail(idOferta: Long, latitude: Double, longitude: Double){
        val alert = Commons.showLoader(this).show()
        val token = SharedPManager.getInstance(this).getLoginToken

        val localOfertaRequest = LocalOfertaRequest(SharedPManager.getInstance(this).getDocumentForLogin, null, idOferta, latitude, longitude, 0)

        ofertaService.getOfertaDetalle("Bearer $token", localOfertaRequest).enqueue(object:
            Callback<OfertasDetalles> {
            override fun onFailure(call: Call<OfertasDetalles>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<OfertasDetalles>, response: Response<OfertasDetalles>) {
                alert.dismiss()
                val ofertas: OfertasDetalles? = response.body()
                if(ofertas != null){
                    if(ofertas.data != null){
                        Glide.with(this@DetailActivity)
                            .asBitmap()
                            .load(ofertas.data.ofertaImagen)
                            .centerCrop()
                            .into(ivOferta)
                        tvDurationBody.text = String.format("Vigencia del %s al %s", ofertas.data.validoDesdeOferta, ofertas.data.validoHastaOferta)
                        tvDescBody.text = ofertas.data.ofertaDescripcion

                        var texto = ""
                        for (i in ofertas.data.listaLocales){
                            texto += i.referenciaLocal + ", "
                        }
                        tvLocalBody.text = texto.substring(0, texto.length - 2)
                    }
                }
            }
        })
    }

}


