package com.hackatonbbva.geofertas.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.adapters.EmpresasAdapter
import com.hackatonbbva.geofertas.data.RetrofitApiClient
import com.hackatonbbva.geofertas.data.api.EmpresaApi
import com.hackatonbbva.geofertas.models.EmpresaCercanaRequest
import com.hackatonbbva.geofertas.models.Empresas
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.GeoLocalizacion
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_empresas.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EmpresasActivity : AppCompatActivity() {

    private lateinit var geoLoc: GeoLocalizacion
    private lateinit var empresaService: EmpresaApi
    private lateinit var empresasAdapter: EmpresasAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_empresas)
        geoLoc = GeoLocalizacion()
        val posicion = geoLoc.getActualPosition(this)
        empresaService = RetrofitApiClient.createRetrofitService(EmpresaApi::class.java)
        initToolbar()
        initRecycler()
        loadEmpresas(posicion.latitud, posicion.longitud)
    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
    }

    private fun initRecycler(){
        empresasAdapter = EmpresasAdapter(this)
        recyclerEmpresas.adapter = empresasAdapter
        recyclerEmpresas.setHasFixedSize(true)
        recyclerEmpresas.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun loadEmpresas(latitude: Double, longitude: Double){
        val alert = Commons.showLoader(this).show()
        val empresaCercanaRequest = EmpresaCercanaRequest(SharedPManager.getInstance(this).getDocumentForLogin, latitude, longitude, 0)
        val token = SharedPManager.getInstance(this).getLoginToken
        empresaService.getEmpresas("Bearer $token", empresaCercanaRequest).enqueue(object:
            Callback<Empresas> {
            override fun onFailure(call: Call<Empresas>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<Empresas>, response: Response<Empresas>) {
                alert.dismiss()
                val empresas: Empresas? = response.body()

                if(empresas != null){
                    if(empresas.data.count() > 0){
                        empresasAdapter.addEmpresas(empresas.data)
                    }
                }
            }
        })
    }


}