package com.hackatonbbva.geofertas.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.hackatonbbva.geofertas.R
import com.hackatonbbva.geofertas.adapters.LocalesAdapter
import com.hackatonbbva.geofertas.data.RetrofitApiClient
import com.hackatonbbva.geofertas.data.api.LocalApi
import com.hackatonbbva.geofertas.models.Empresa
import com.hackatonbbva.geofertas.models.LocalOfertaRequest
import com.hackatonbbva.geofertas.models.Locales
import com.hackatonbbva.geofertas.utils.Commons
import com.hackatonbbva.geofertas.utils.GeoLocalizacion
import com.hackatonbbva.geofertas.utils.SharedPManager
import kotlinx.android.synthetic.main.activity_locales.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocalesActivity : AppCompatActivity() {

    private lateinit var geoLoc: GeoLocalizacion
    private lateinit var localService: LocalApi
    private lateinit var localesAdapter: LocalesAdapter
    private lateinit var selectedEmpresa: Empresa

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_locales)
        selectedEmpresa = intent?.extras?.get("selected_empresa") as Empresa

        geoLoc = GeoLocalizacion()
        val posicion = geoLoc.getActualPosition(this)
        localService = RetrofitApiClient.createRetrofitService(LocalApi::class.java)
        initToolbar()
        initRecycler()
        loadLocals(posicion.latitud, posicion.longitud, selectedEmpresa.idEmpresa)
        tvCategoryTitle.text = selectedEmpresa.nombreEmpresa
    }

    private fun initToolbar(){
        setSupportActionBar(toolbar)
    }

    private fun initRecycler(){
        localesAdapter = LocalesAdapter(this, selectedEmpresa.nombreEmpresa)
        recyclerLocales.adapter = localesAdapter
        recyclerLocales.setHasFixedSize(true)
        recyclerLocales.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun loadLocals(latitude: Double, longitude: Double, idEmpresa: Long){
        val alert = Commons.showLoader(this).show()

        val localOfertaRequest = LocalOfertaRequest(SharedPManager.getInstance(this).getDocumentForLogin, idEmpresa, null, latitude, longitude, 0)

        val token = SharedPManager.getInstance(this).getLoginToken
        localService.getLocalesCercanos("Bearer $token", localOfertaRequest).enqueue(object:
            Callback<Locales> {
            override fun onFailure(call: Call<Locales>, t: Throwable) {
                alert.dismiss()
                t.printStackTrace()
            }

            override fun onResponse(call: Call<Locales>, response: Response<Locales>) {
                alert.dismiss()
                val locales: Locales? = response.body()
                Log.d("locales", locales.toString())
                if(locales != null){
                    if(locales.data.count() > 0){
                        localesAdapter.addLocales(locales.data)
                    }
                }
            }
        })
    }


}